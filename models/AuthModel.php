<?php

class AuthModel extends db {

    private $table = 'users', $db;

    public function __construct() {
        $this->db = $this->connect();
    }

    function isLogged($username, $accessKey) {
        $sql = $this->db->prepare('SELECT * FROM ' . $this->table . ' WHERE username like ? and accessKey like ? ');
        $sql->bind_param("ss", $username, $accessKey);
        $sql->execute();
        $result = $sql->get_result();

        if ($result->num_rows > 0) {
            $logged = true;
        } else {
            $logged = false;
        }
        return $logged;
    }

    function login($username, $password) {
        $sql = $this->db->prepare('SELECT * FROM ' . $this->table . ' WHERE username like ? and password like ? ');
        $sql->bind_param("ss", $username, $password);
        $sql->execute();
        $result = $sql->get_result();

        if ($result->num_rows > 0) {
            $logged = true;
        } else {
            $logged = false;
        }
        return $logged;
    }

    function saveAccessKey($username, $accessKey) {
        $sql = $this->db->prepare('UPDATE ' . $this->table . ' SET accessKey = ? WHERE username LIKE ?');
        $sql->bind_param("ss", $accessKey, $username);
        $sql->execute();
    }

}
