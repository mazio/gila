<?php

class SedanModel extends db {

    private $table = 'sedan', $db;

    public function __construct() {
        $this->db = $this->connect();
    }

    public function addSedan(Sedan $sedan) {
        $sql = $this->db->prepare('INSERT INTO ' . $this->table . ' ( wheels, engine, color, brand, doors, model ) VALUES (?,?,?,?,?,?)');
        $sql->bind_param("idssis", $sedan->wheelsNumber, $sedan->enginePower, $sedan->color, $sedan->brand, $sedan->doors, $sedan->model);
        $sql->execute();
    }

    public function getSedan() {
        $arr = [];
        $sql = "SELECT * FROM " . $this->table;
        $result = $this->db->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $arr[] = $row;
            }
        }
        return $arr;
    }

}
