<?php

class MotorcycleModel extends db {

    private $table = 'motorcycles', $db;

    public function __construct() {
        $this->db = $this->connect();
    }

    public function addMotorcycle(Motorcycle $motorcycle) {
        $sql = $this->db->prepare('INSERT INTO ' . $this->table . ' ( wheels, engine, color, brand ) VALUES (?,?,?,?)');
        $sql->bind_param("idss", $motorcycle->wheelsNumber, $motorcycle->enginePower, $motorcycle->color, $motorcycle->brand);
        $sql->execute();
    }

    public function getMotorcycles() {
        $arr = [];
        $sql = "SELECT * FROM " . $this->table;
        $result = $this->db->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $arr[] = $row;
            }
        }
        return $arr;
    }

}
