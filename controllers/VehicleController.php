<?php

class VehicleController extends Controller {

    function __construct() {
        
    }

    function validateMoto() {
        $this->json();
        $resp['valid'] = false;
        $resp['error'] = "Validation error";
        if (isset($_POST['wheels'], $_POST['engine'], $_POST['color'], $_POST['brand'])) {
            $wheelsNumber = intval($_POST['wheels']);
            $enginePower = floatval($_POST['engine']);
            $color = $_POST['color'];
            $brand = $_POST['brand'];

            if (!empty($wheelsNumber) && !empty($enginePower) && !empty($color) && !empty($brand)) {
                if (is_int($wheelsNumber) && is_float($enginePower) && is_string($color) && is_string($brand)) {
                    $this->addVehicle(new MotorcycleFactory($wheelsNumber, $enginePower, $color, $brand));
                    $resp['error'] = "";
                    $resp['valid'] = true;
                }
            }
        }

        echo json_encode($resp);
    }

    function validateSedan() {
        $this->json();
        $resp['valid'] = false;
        $resp['error'] = "Validation error";
        if (isset($_POST['wheels'], $_POST['engine'], $_POST['color'], $_POST['brand'], $_POST['doors'], $_POST['model'])) {
            $wheelsNumber = intval($_POST['wheels']);
            $enginePower = floatval($_POST['engine']);
            $color = $_POST['color'];
            $brand = $_POST['brand'];
            $doors = intval($_POST['doors']);
            $model = $_POST['model'];

            if (!empty($wheelsNumber) && !empty($enginePower) && !empty($color) && !empty($brand) && !empty($doors) && !empty($model)) {
                if (is_int($wheelsNumber) && is_float($enginePower) && is_string($color) && is_string($brand) && is_int($doors) && is_string($model)) {
                    $this->addVehicle(new SedanFactory($wheelsNumber, $enginePower, $color, $brand, $doors, $model));
                    $resp['error'] = "";
                    $resp['valid'] = true;
                }
            }
        }
        echo json_encode($resp);
    }

    function addVehicle(VehicleFactory $vehicle) {
        $vehicle->addVehicle();
    }

    function getVehicle(VehicleFactory $vehicle) {
        $this->json();
        $data = $vehicle->getAllVehicles();
        echo json_encode($data);
    }

    function dashboardView() {
        include_once '../public/views/dashboard.php';
    }

    function addMotorcycleView() {
        include_once '../public/views/addMotorcycle.php';
    }

    function addSedanView() {
        include_once '../public/views/addSedan.php';
    }

    function showMotorcycleView() {
        include_once '../public/views/showMotorcycles.php';
    }

    function showSedanView() {
        include_once '../public/views/showSedan.php';
    }

}
