<?php

class AuthController extends Controller {

    function __construct() {
        
    }

    function loginView() {
        include '../public/views/login.php';
    }

    function loginProcess() {
        $this->json();

        $resp = $this->respInitialize();
        if (isset($_POST['username'], $_POST['password'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];                

            $authModel = new AuthModel();
            $resp['logged'] = $authModel->login($username, $password);

            if ($resp['logged']) {
                $resp['username'] = $username;
                $resp['accessKey'] = rand(10000000, 99999999);
                $authModel->saveAccessKey($username, $resp['accessKey']);
                $_SESSION['logged_in'] = true;
            }
        }

        echo json_encode($resp);
    }


    private function respInitialize() {
        $resp['logged'] = false;
        $resp['accessKey'] = false;
        $resp['username'] = false;
        return $resp;
    }

    function destroySession() {
        $_SESSION['logged_in'] = false;
    }

}
