var vehicle = {
    start: function start() {
        this.addMoto();
        this.addSedan();
        var urlParams = new URLSearchParams(window.location.search);
        var action = urlParams.get('action');

        if (action == 'showMotorcycle') {
            this.showMotorcycle();
        }
        if (action == 'showSedan') {
            this.showSedan();
        }
    },
    addMoto: function addMoto() {
        $('#motoForm').on('submit', function () {
            event.preventDefault();
            var data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "index.php?action=validateMoto",
                data: data,
                dataType: "json",
                success: function (response) {
                    if (response.error != "") {
                        $('#errors').html(response.error);
                    } else {
                        $('#errors').html("");
                        if (response.valid == true) {
                            $('#errors').html("Motorcycle was saved");
                            $('#motoForm').hide();
                        }
                    }
                },
                error: function (obj, error) {
                    console.log("Error: " + error);
                },
            });
        });
    },
    addSedan: function addSedan() {
        $('#sedanForm').on('submit', function () {
            event.preventDefault();
            var data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: "index.php?action=validateSedan",
                data: data,
                dataType: "json",
                success: function (response) {
                    if (response.error != "") {
                        $('#errors').html(response.error);
                    } else {
                        $('#errors').html("");
                        if (response.valid == true) {
                            $('#errors').html("Sedan was saved");
                            $('#sedanForm').hide();
                        }
                    }
                },
                error: function (obj, error) {
                    console.log("Error: " + error);
                },
            });
        });
    },
    showMotorcycle: function showMotorcycle() {
        $.ajax({
            type: "GET",
            url: "index.php?action=getMotorcycle",
            dataType: "json",
            success: function (response) {
                $('#content').html(JSON.stringify(response));
            },
            error: function (obj, error) {
                console.log("Error: " + error);
            },
        });
    },
    showSedan: function showSedan() {
        $.ajax({
            type: "GET",
            url: "index.php?action=getSedan",
            dataType: "json",
            success: function (response) {
                $('#content').html(JSON.stringify(response));
            },
            error: function (obj, error) {
                console.log("Error: " + error);
            },
        });
    }
}
$(document).ready(function () {

    vehicle.start();

});

