var auth = {
    start: function start() {
        this.login();        
    },
    login: function login() {
        $('form').on('submit', function () {
            event.preventDefault();
            var username = $('#uname').val();
            var password = $('#psw').val();
            $.ajax({
                type: "POST",
                url: "index.php?action=login",
                data: "password=" + password + "&username=" + username,
                dataType: "json",
                success: function (response) {
                    if (response.logged == true) {
                        window.location.href='index.php?action=dashboard';
                    } else {
                        $('#errors').html("Incorrect User and Password");
                    }
                },
                error: function (obj, error) {
                    console.log("Error: " + error);
                },
            });
        });
    }
}
$(document).ready(function () {

    auth.start();

});

