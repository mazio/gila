<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>

        <h1>Add Sedan</h1>
        <div id="errors"></div>
        <form id="sedanForm" action="#" method="post">

            <div class="container">                
                <label for="engine"><b>Engine Power</b></label>
                <input type="text" placeholder="Enter Engine Power" id="engine" name="engine">

                <label for="wheels"><b>Wheels</b></label>
                <input type="text" placeholder="Enter Wheels" id="wheels" name="wheels">                

                <label for="doors"><b>Doors</b></label>
                <input type="text" placeholder="Enter Doors" id="brand" name="doors">

                <label for="color"><b>Color</b></label>
                <input type="tex" placeholder="Enter Color" id="color" name="color">

                <label for="brand"><b>Brand</b></label>
                <input type="text" placeholder="Enter Brand" id="brand" name="brand"> 

                <label for="model"><b>Model</b></label>
                <input type="text" placeholder="Enter Model" id="model" name="model">                 

                <button id="addSedan" type="submit">Add</button>  
            </div>

        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="js/vehicle.js?v=<?php echo rand(); ?>"></script>

    </body>
</html>

