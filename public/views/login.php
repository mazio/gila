<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>

        <h1>Login</h1>
        <form action="#" method="post">

            <div class="container">
                <div id="errors"></div>
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" id="uname" name="uname">

                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" id="psw" name="psw">

                <button id="login" type="submit">Login</button>  
            </div>

        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="js/auth.js?v=<?php echo rand(); ?>"></script>

    </body>
</html>

