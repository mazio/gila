<?php

define("ENV", "dev");
switch (ENV) {
    case 'dev':
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        break;
    case "prod":
        error_reporting(0);
        ini_set('display_errors', 0);
        break;
}

