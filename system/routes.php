<?php

$action = "";
if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
}

$api = false;
foreach (getallheaders() as $name => $value) {
    if ($name == 'x-api-key' && $value == 'ac3637ab-4b52-421c-a7db-e958615b63b4') {
        $api = true;
    } else if ($name == 'x-api-key' && $value != 'ac3637ab-4b52-421c-a7db-e958615b63b4') {
        header('Content-Type: application/json; charset=utf-8');
        $data['error'] = "Invalid API key";
        echo json_encode($data);
        exit;
    }
}


if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] && $api == false) {

    $vehicle = new VehicleController();
    switch ($action) {
        case 'dashboard':
            $vehicle->dashboardView();
            break;
        case 'addMotorcycle':
            $vehicle->addMotorcycleView();
            break;
        case 'validateMoto':
            $vehicle->validateMoto();
            break;
        case 'addSedan':
            $vehicle->addSedanView();
            break;
        case 'validateSedan':
            $vehicle->validateSedan();
            break;
        case 'showMotorcycle':
            $vehicle->showMotorcycleView();
            break;
        case 'showSedan':
            $vehicle->showSedanView();
            break;
        case 'getMotorcycle':
            $vehicle->getVehicle(new MotorcycleFactory());
            break;
        case 'getSedan':
            $vehicle->getVehicle(new SedanFactory());
            break;
        case 'destroySession':
            $auth = new AuthController();
            $auth->destroySession();
            break;
        default :
            $vehicle->dashboardView();
            break;
    }
} else if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] && $api == true) {
    $auth = new AuthController();
    $vehicle = new VehicleController();
    switch ($action) {
        case 'login':
            $auth->loginProcess();
            break;
        case 'addMotorcycle':
            $vehicle->validateMoto();
            break;
        case 'addSedan':
            $vehicle->validateSedan();
            break;
        case 'getMotorcycle':
            $vehicle->getVehicle(new MotorcycleFactory());
            break;
        case 'getSedan':
            $vehicle->getVehicle(new SedanFactory());
            break;
    }
} else {

    $auth = new AuthController();
    switch ($action) {
        case 'login':
            $auth->loginProcess();
            break;
        default :
            $auth->loginView();
    }
}

    