<?php

include_once 'enviroment.php';

//controllers
include_once '../controllers/Controller.php';
include_once '../controllers/AuthController.php';
include_once '../controllers/VehicleController.php';

//classes
include_once '../classes/Vehicle.php';
include_once '../classes/VehicleFactory.php';
include_once '../classes/Motorcycle.php';
include_once '../classes/MotorcycleFactory.php';
include_once '../classes/Sedan.php';
include_once '../classes/SedanFactory.php';

//db
include_once 'db.php';
//models
include_once '../models/AuthModel.php';
include_once '../models/MotorcycleModel.php';
include_once '../models/SedanModel.php';

include_once 'routes.php';

