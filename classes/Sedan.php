<?php

class Sedan implements Vehicle {

    public $wheelsNumber, $enginePower, $color, $brand, $doors, $model;

    public function __construct(int $wheelsNumber, float $enginePower, string $color, string $brand, int $doors, string $model) {
        $this->wheelsNumber = $wheelsNumber;
        $this->enginePower = $enginePower;
        $this->color = $color;
        $this->brand = $brand;
        $this->doors = $doors;
        $this->model = $model;
    }

    public function show(): Array {
        $sedanModel = new SedanModel();
        $arrSedan = $sedanModel->getSedan();
        return $arrSedan;
    }

    public function add(): bool {
        $sedanModel = new SedanModel();
        $sedanModel->addSedan($this);
        return true;
    }

}
