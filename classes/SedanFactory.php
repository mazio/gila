<?php

class SedanFactory extends VehicleFactory {

    private $tiresNumber, $enginePower;

    public function __construct(int $wheelsNumber = 0, float $enginePower = 0, string $color = "", string $brand = "", int $doors = 0, string $model = "") {
        $this->wheelsNumber = $wheelsNumber;
        $this->enginePower = $enginePower;
        $this->color = $color;
        $this->brand = $brand;
        $this->doors = $doors;
        $this->model = $model;
    }

    public function getVehicle(): Vehicle {
        return new Sedan($this->wheelsNumber, $this->enginePower, $this->color, $this->brand, $this->doors, $this->model);
    }

}
