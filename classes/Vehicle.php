<?php

interface Vehicle
{        
    public function show(): Array;

    public function add(): bool;

}
