<?php

abstract class VehicleFactory {

    abstract public function getVehicle(): Vehicle;

    public function addVehicle(): bool {
        $vehicle = $this->getVehicle();

        $result = $vehicle->add();

        return $result;
    }

    public function getAllVehicles(): Array {
        $vehicle = $this->getVehicle();
        $result = $vehicle->show();
        return $result;
    }

}
