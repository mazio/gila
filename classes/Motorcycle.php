<?php

class Motorcycle implements Vehicle {

    public $wheelsNumber, $enginePower, $color, $brand;

    public function __construct(int $wheelsNumber=0, float $enginePower=0, string $color="", string $brand="") {
        $this->wheelsNumber = $wheelsNumber;
        $this->enginePower = $enginePower;
        $this->color = $color;
        $this->brand = $brand;
    }

    public function show(): Array {
        $motorcycleModel = new MotorcycleModel();
        $arrMoto = $motorcycleModel->getMotorcycles();
        return $arrMoto;
    }

    public function add(): bool {
        $motorcycleModel = new MotorcycleModel();
        $motorcycleModel->addMotorcycle($this);
        return true;
    }

}
