<?php

class MotorcycleFactory extends VehicleFactory {

    private $wheelsNumber, $enginePower, $color, $brand;

    public function __construct(int $wheelsNumber=0, float $enginePower=0, string $color='', string $brand='') {
        $this->wheelsNumber = $wheelsNumber;
        $this->enginePower = $enginePower;
        $this->color = $color;
        $this->brand = $brand;
    }

    public function getVehicle(): Vehicle {
        return new Motorcycle($this->wheelsNumber, $this->enginePower, $this->color, $this->brand);
    }

}
